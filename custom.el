(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("0754c176c3f850e1f90e177e283247749a1ac688faebf05b6016343cb3f00064" "10a31b6c251640d04b2fa74bd2c05aaaee915cbca6501bcc82820cdc177f5a93" default))
 '(initial-frame-alist '((fullscreen . maximized)))
 '(package-selected-packages
   '(k8s-mode cfn-mode vagrant-tramp company-terraform markdown-mode terraform-mode smart-shift yaml-mode paredit load-dir company-go go-mode smartparens ws-butler dtrt-indent clean-aindent-mode virtualenvwrapper elpy vagrant yasnippet-snippets yasnippet company-c-headers sr-speedbar helm-gtags helm-projectile helm-swoop helm-themes helm macrostep flycheck projectile multi-term company iedit ssh-agency magit nyan-mode northcode-theme flycheck-yamllint load-directory company-c-backends smart-parens-config company-irony-c-headers)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-indentation-face ((t (:inherit fringe :background "dark gray"))))
 '(term-color-blue ((t (:foreground "#00bfff" :background "#00bfff")))))
