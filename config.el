(setq user-full-name "Brad Stinson"
      user-mail-address "brad@bradthebuilder.me")

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

;;(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(add-to-list 'package-archives
            '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;(add-to-list 'package-archives
;'("elpy" . "http://jorgenschaefer.github.io/packages/") t)
(package-refresh-contents)

(let ((default-directory  "~/.emacs.d/lisp/"))
  (normal-top-level-add-subdirs-to-load-path))

;; These functions are useful. Activate them.
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

;; Answering just 'y' or 'n' will do
(defalias 'yes-or-no-p 'y-or-n-p)

;; Keep all backup and auto-save files in one directory
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

;; UTF-8 please
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top
(setq-default indent-tabs-mode nil)

;; Turn off the blinking cursor
(blink-cursor-mode -1)

(setq-default indent-tabs-mode nil)
(setq-default indicate-empty-lines t)

;; Don't count two spaces after a period as the end of a sentence.
;; Just one space is needed.
(setq sentence-end-double-space nil)

;; delete the region when typing, just like as we expect nowadays.
(delete-selection-mode t)

(show-paren-mode t)

(column-number-mode t)

(global-visual-line-mode)
(diminish 'visual-line-mode)

(setq uniquify-buffer-name-style 'forward)

;; -i gets alias definitions from .bash_profile
;(setq shell-command-switch "-ic")

;; Don't beep at me
(setq visible-bell t)

(defun occur-dwim ()
  "Call `occur' with a sane default."
  (interactive)
  (push (if (region-active-p)
            (buffer-substring-no-properties
             (region-beginning)
             (region-end))
          (thing-at-point 'symbol))
        regexp-history)
  (call-interactively 'occur))

  (bind-key "M-s o" 'occur-dwim)

(use-package northcode-theme
    :ensure t
  )
(load-theme 'northcode)

;(defvar ha/fixed-font-family
 ;     (cond 
 ;           ((x-list-fonts "Source Code Pro") "Source Code Pro"))
 ;     "My fixed width font based on what is installed, `nil' if not defined.")
;(defvar ha/variable-font-tuple
;      (cond ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
;            (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro.")))
;      "My variable width font available to org-mode files and whatnot.")
;(set-face-attribute 'default t :font "Source Code Pro")
;    (add-to-list 'default-frame-alist '(font . "Source Code Pro" ))

(setq org-startup-indented t)

(use-package nyan-mode
  :ensure t
  :init
  (nyan-mode)
)

(use-package magit
  :ensure t
)
(use-package ssh-agency
  :ensure t
)
(global-set-key (kbd "C-x g") 'magit-status)
(setq ssh-agency-add-executable "/usr/bin/ssh-add")
(setq ssh-agency-agent-executable "/usr/bin/ssh-agent")
(setenv "SSH_ASKPASS" "ssh-askpass")

(use-package iedit
  :ensure t
)
(define-key global-map (kbd "C-c ;") 'iedit-mode)

(use-package company
  :ensure t
)

(use-package multi-term
  :ensure t
)
(setq multi-term-program "/bin/zsh")
(add-hook 'term-mode-hook
          (lambda ()
            (setq term-buffer-maximum-size 1000)))
(add-hook 'term-mode-hook
          (lambda ()
            (add-to-list 'term-bind-key-alist '("M-[" . multi-term-prev))
            (add-to-list 'term-bind-key-alist '("M-]" . multi-term-next))))
(global-set-key (kbd "C-c t") 'multi-term)
            ;Adding 'term-line-mode as a hook does not work. Keybinding the mode also fails. =/
            ;These 2 functions used to work, but no longer. I have to manually switch to term-line-mode
            ;in order to get access to Emacs keybindings (including mark and kill), then return to
            ;term-char-mode
            ;(add-to-list 'term-bind-key-alist '("C-<SPC>" . set-mark-command))
            ;(add-to-list 'term-bind-key-alist '("M-w" . kill-ring-save))))

(use-package projectile
  :ensure t
)

(use-package flycheck
  :ensure t
)

(use-package macrostep
  :ensure t
)

(use-package helm
   :ensure t 
   :init
   (progn
     (require 'helm-config)
     (require 'helm-grep)
     (use-package helm-themes
     :ensure t
     )
     (defun helm-hide-minibuffer-maybe ()
       (when (with-helm-buffer helm-echo-input-in-header-line)
         (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
           (overlay-put ov 'window (selected-window))
           (overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
                                   `(:background ,bg-color :foreground ,bg-color)))
           (setq-local cursor-type nil))))

     (add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)
     ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
     ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
     ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
     (global-set-key (kbd "C-c h") 'helm-command-prefix)
     (global-unset-key (kbd "C-x c"))

     (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
     (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
     (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

     (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
     (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
     (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)

     (when (executable-find "curl")
       (setq helm-google-suggest-use-curl-p t))

     (setq helm-google-suggest-use-curl-p t
           helm-scroll-amount 4 ; scroll 4 lines other window using M-<next>/M-<prior>
           ;; helm-quick-update t ; do not display invisible candidates
           helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.

           ;; you can customize helm-do-grep to execute ack-grep
           ;; helm-grep-default-command "ack-grep -Hn --smart-case --no-group --no-color %e %p %f"
           ;; helm-grep-default-recurse-command "ack-grep -H --smart-case --no-group --no-color %e %p %f"
           helm-split-window-in-side-p t ;; open helm buffer inside current window, not occupy whole other window

           helm-echo-input-in-header-line t

           ;; helm-candidate-number-limit 500 ; limit the number of displayed canidates
           helm-ff-file-name-history-use-recentf t
           helm-move-to-line-cycle-in-source t ; move to end or beginning of source when reaching top or bottom of source.
           helm-buffer-skip-remote-checking t

           helm-mode-fuzzy-match t

           helm-buffers-fuzzy-matching t ; fuzzy matching buffer names when non-nil
                                         ; useful in helm-mini that lists buffers
           helm-org-headings-fontify t
          ;; helm-find-files-sort-directories t
           ;; ido-use-virtual-buffers t
           helm-semantic-fuzzy-match t
           helm-M-x-fuzzy-match t
           helm-imenu-fuzzy-match t
           helm-lisp-fuzzy-completion t
           ;; helm-apropos-fuzzy-match t
           helm-buffer-skip-remote-checking t
           helm-locate-fuzzy-match t
           helm-display-header-line nil)

     (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

     (global-set-key (kbd "M-x") 'helm-M-x)
     (global-set-key (kbd "M-y") 'helm-show-kill-ring)
     (global-set-key (kbd "C-x b") 'helm-buffers-list)
     (global-set-key (kbd "C-x C-f") 'helm-find-files)
     (global-set-key (kbd "C-c r") 'helm-recentf)
     (global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
     (global-set-key (kbd "C-c h o") 'helm-occur)

     (global-set-key (kbd "C-c h w") 'helm-wikipedia-suggest)
     (global-set-key (kbd "C-c h g") 'helm-google-suggest)

     (global-set-key (kbd "C-c h x") 'helm-register)
     (global-set-key (kbd "C-x r j") 'jump-to-register)

     (define-key 'help-command (kbd "C-f") 'helm-apropos)
     (define-key 'help-command (kbd "r") 'helm-info-emacs)
     (define-key 'help-command (kbd "C-l") 'helm-locate-library)

     ;; use helm to list eshell history
     (add-hook 'eshell-mode-hook
               #'(lambda ()
                   (define-key eshell-mode-map (kbd "M-l")  'helm-eshell-history)))

     ;; Save current position to mark ring
     (add-hook 'helm-goto-line-before-hook 'helm-save-current-pos-to-mark-ring)

     ;; show minibuffer history with Helm
     (define-key minibuffer-local-map (kbd "M-p") 'helm-minibuffer-history)
     (define-key minibuffer-local-map (kbd "M-n") 'helm-minibuffer-history)

     (define-key global-map [remap find-tag] 'helm-etags-select)

     (define-key global-map [remap list-buffers] 'helm-buffers-list)

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; PACKAGE: helm-swoop                ;;
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Locate the helm-swoop folder to your path
     (use-package helm-swoop
       :ensure t
       :bind (("C-c h o" . helm-swoop)
              ("C-c s" . helm-multi-swoop-all))
       :config
       ;; When doing isearch, hand the word over to helm-swoop
       (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)

       ;; From helm-swoop to helm-multi-swoop-all
       (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)

       ;; Save buffer when helm-multi-swoop-edit complete
       (setq helm-multi-swoop-edit-save t)

       ;; If this value is t, split window inside the current window
       (setq helm-swoop-split-with-multiple-windows t)

       ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
       (setq helm-swoop-split-direction 'split-window-vertically)

       ;; If nil, you can slightly boost invoke speed in exchange for text color
       (setq helm-swoop-speed-or-color t))

     (helm-mode 1)

     (use-package helm-projectile
       :ensure t
       :init
       (helm-projectile-on)
       (setq projectile-completion-system 'helm)
       (setq projectile-indexing-method 'alien))))

(provide 'setup-helm)

(require 'setup-helm)
(use-package helm-gtags
:ensure t
:config
(setq
helm-gtags-ignore-case t
helm-gtags-auto-update t
helm-gtags-use-input-at-cursor t
helm-gtags-pulse-at-cursor t
helm-gtags-prefix-key "\C-cg"
helm-gtags-suggested-key-mapping t
))

(add-hook 'dired-mode-hook 'helm-gtags-mode)
(add-hook 'eshell-mode-hook 'helm-gtags-mode)
(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)
(add-hook 'asm-mode-hook 'helm-gtags-mode)

(define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
(define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
(define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
(define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
(define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
(define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)

(use-package sr-speedbar
  :ensure t
)
(define-key helm-gtags-mode-map (kbd "C-c g s") 'sr-speedbar-toggle)
;(global-set-key (kbd "C-c f") 'sr-speedbar-toggle)

(use-package company
  :ensure t
)
(add-hook 'after-init-hook 'global-company-mode)

;; Company-c-headers installed manually
;;(require 'company-c-headers)
;; Company-c-headers installed from melpa
(use-package company-c-headers
  :ensure t
)

(add-to-list 'company-backends 'company-c-headers)
(add-to-list 'company-c-headers-path-system "/usr/include/c++/8/")

(use-package yasnippet
  :ensure t
)
(yas-global-mode 1)
(use-package yasnippet-snippets
  :ensure t
)

(setq tramp-default-method "ssh")

;;(setq tramp-verbose 6)
(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))

(setq tramp-shell-prompt-pattern "\\(?:^\\|\r\\)[^]#$%>\n]*#?[]#$%>].* *\\(^[\\[[0-9;]*[a-zA-Z] *\\)*")

(use-package vagrant
  :ensure t
)

(use-package vagrant-tramp
  :ensure t
)

(use-package elpy
  :ensure t
)
(elpy-enable)
(define-key yas-minor-mode-map (kbd "C-c k") 'yas-expand)
(setq elpy-rpc-python-command "/usr/bin/python3")

(use-package virtualenvwrapper
  :ensure t
)
;(venv-initialize-eshell)
(venv-initialize-interactive-shells)
;(setq venv-location (getenv "WORKON_HOME"))
(setq venv-location (shell-command-to-string ". ~/.zshrc; echo -n $WORKON_HOME"))
;(setq venv-project-home (getenv "PROJECT_HOME"))
(setq venv-project-home (shell-command-to-string ". ~/.zshrc; echo -n $PROJECT_HOME"))
(setenv "WORKON_HOME" venv-location)
(setenv "PROJECT_HOME" venv-project-home)
(setq-default mode-line-format (cons '(:exec venv-current-name) mode-line-format))
(setq eshell-prompt-function
 (lambda ()
   (concat "(" venv-current-name ")" (eshell/pwd) " $ ")))
(add-hook 'venv-postactivate-hook
  (lambda () (projectile-mode)))
(add-hook 'venv-postactivate-hook
  (lambda () (eshell/cd (concat venv-project-home "/" venv-current-name))))

(add-hook 'c-mode-common-hook 'hs-minor-mode)

(setq
 c-default-style "linux" ;; set style to "linux"
 )

; automatically indent when press RET
(global-set-key (kbd "RET") 'newline-and-indent)

;; activate whitespace-mode to view all whitespace characters
(global-set-key (kbd "C-c w") 'whitespace-mode)

;; show unncessary whitespace that can mess up your diff
(add-hook 'prog-mode-hook (lambda () (interactive) (setq show-trailing-whitespace 1)))

;; use space to indent by default
(setq-default indent-tabs-mode nil)

;; set appearance of a tab that is represented by 4 spaces
(setq-default tab-width 4)

;; Clean-aindent available in melpa
(use-package clean-aindent-mode
  :ensure t
)
(add-hook 'prog-mode-hook 'clean-aindent-mode)

(use-package dtrt-indent
  :ensure t
)
(dtrt-indent-mode 1)

(use-package ws-butler
  :ensure t
)
(add-hook 'c-mode-common-hook 'ws-butler-mode)

(use-package smartparens
  :ensure t
)
(show-smartparens-global-mode +1)
(smartparens-global-mode 1)

;; when you press RET, the curly braces automatically
;; add another newline
(sp-with-modes '(c-mode c++-mode)
  (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))
  (sp-local-pair "/*" "*/" :post-handlers '((" | " "SPC")
                                            ("* ||\n[i]" "RET"))))

;; Set F5 to compile with debug symbols by default
(global-set-key (kbd "<f5>") (lambda ()
                               (interactive)
                               (setq-local compile-command "make -k DEBUG=1")
                               (setq-local compilation-read-command nil)
                               (call-interactively 'compile)))

;; Set F6 as compile hotkey without debug symbols (Release build)
(global-set-key (kbd "<f6>") (lambda ()
                               (interactive)
                               (setq-local compilation-read-command nil)
                               (call-interactively 'compile)))


;; Setup GDB and GUD for bebugging
(setq
 ;; use gdb-many-windows by default
 gdb-many-windows t

 ;; Non-nil means display source file containing the main routine at startup
 gdb-show-main t
 )

(use-package go-mode
  :ensure t
)
(use-package company-go
  :ensure t
)
(add-hook 'before-save-hook 'gofmt-before-save)
(add-hook 'go-mode-hook (lambda ()
                          (local-set-key (kbd "C-c C-r") 'go-remove-unused-imports)))

(use-package load-dir
  :ensure t
  :config
  (load-dir-one "~/.emacs.d/test")
)

(use-package paredit
  :ensure t
  :config
  (add-hook 'emacs-lisp-mode-hook #'paredit-mode)
  ;; enable in the *scratch* buffer
  (add-hook 'lisp-interaction-mode-hook #'paredit-mode)
  (add-hook 'ielm-mode-hook #'paredit-mode)
  (add-hook 'lisp-mode-hook #'paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'paredit-mode)
  (add-hook 'scheme-mode-hook #'enable-paredit-mode)
)
(add-hook 'after-save-hook 'check-parens nil t)

(add-hook 'html-mode-hook 'yas-minor-mode)

(use-package yaml-mode
  :ensure t
)
(add-to-list 'load-path "~/.emacs.d/lisp/")
(load "highlight-indentation")
(add-hook 'yaml-mode-hook 'highlight-indentation-mode)
(add-hook 'yaml-mode-hook 'flycheck-mode)
;; SmartShift available in melpa
(use-package smart-shift
  :ensure t
)
(global-smart-shift-mode 1)

(defun aj-toggle-fold ()
  "Toggle fold all lines larger than indentation on current line"
  (interactive)
  (let ((col 1))
    (save-excursion
      (back-to-indentation)
      (setq col (+ 1 (current-column)))
      (set-selective-display
       (if selective-display nil (or col 1))))))
(global-set-key (kbd "C-c f") 'aj-toggle-fold)

(setq flycheck-yaml-yamllint-executable "/usr/local/bin/yamllint")

(use-package terraform-mode
  :ensure t
)

(use-package k8s-mode
  :ensure t
  :hook (k8s-mode . yas-minor-mode)
)

(use-package markdown-mode
  :ensure t
)

(use-package cfn-mode
  :ensure t
)
(when (featurep 'yaml-mode)

  (define-derived-mode cfn-mode yaml-mode
    "Cloudformation"
    "Cloudformation template mode.")

  (add-to-list 'magic-mode-alist
               '("\\(---\n\\)?AWSTemplateFormatVersion:" . cfn-mode))

  (when (featurep 'flycheck)
    (flycheck-define-checker cfn-lint
      "AWS CloudFormation linter using cfn-lint.

Install cfn-lint first: pip install cfn-lint

See `https://github.com/aws-cloudformation/cfn-python-lint'."

      :command ("cfn-lint" "-f" "parseable" source)
      :error-patterns ((warning line-start (file-name) ":" line ":" column
                                ":" (one-or-more digit) ":" (one-or-more digit) ":"
                                (id "W" (one-or-more digit)) ":" (message) line-end)
                       (error line-start (file-name) ":" line ":" column
                              ":" (one-or-more digit) ":" (one-or-more digit) ":"
                              (id "E" (one-or-more digit)) ":" (message) line-end))
      :modes (cfn-mode))

    (add-to-list 'flycheck-checkers 'cfn-lint)
    (add-hook 'cfn-mode-hook 'flycheck-mode)))

;(require 'esv)
  ; C-c e looks up a passage and displays it in a pop-up window
 ; (define-key global-map [(control c) ?e] 'esv-passage)
  ; C-c i inserts an ESV passage in plain-text format at point
  ;(define-key global-map [(control c) ?i] 'esv-insert-pa)
  ;(with-temp-buffer
  ;(insert-file-contents "~/.emacs.d/lisp/esvToken.txt")
  ;(setq esv-key (when (string-match "\\(.*\\)" (buffer-string))
  ;        (match-string 1 (buffer-string)))))

(define-prefix-command 'lisp-functional-map)
(global-set-key (kbd "C-c e") 'lisp-functional-map)
(global-set-key (kbd "C-c e b") 'eval-buffer)
(global-set-key (kbd "C-c e e") 'toggle-debug-on-error)
(global-set-key (kbd "C-c e f") 'emacs-lisp-byte-compile-and-load)
(global-set-key (kbd "C-c e r") 'eval-region)

(define-prefix-command 'lisp-find-map)
(global-set-key (kbd "C-h e") 'lisp-find-map)

(global-set-key (kbd "C-h e e") 'view-echo-area-messages)
(global-set-key (kbd "C-h e f") 'find-function)
(global-set-key (kbd "C-h e k") 'find-function-on-key)
(global-set-key (kbd "C-h e l") 'find-library)
(global-set-key (kbd "C-h e v") 'find-variable)
(global-set-key (kbd "C-h e V") 'apropos-value)

(global-set-key (kbd "C-c e m") 'macrostep-expand)
